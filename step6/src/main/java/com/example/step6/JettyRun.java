package com.example.step6;
import com.example.step6.controllers.*;
import com.example.step6.dao.JdbcLikedDao;
import com.example.step6.dao.JdbcMessageDao;
import com.example.step6.dao.JdbcProfileDao;
import com.example.step6.dao.LikedDao;
import com.example.step6.domain.TemplateEngine;
import com.example.step6.service.DefaultLikedService;
import com.example.step6.service.DefaultMessageService;
import com.example.step6.service.DefaultProfileService;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

import java.util.ArrayList;


public class JettyRun  {
    public static void main(String[] args) throws Exception {
        Server server = new Server(8081);
        TemplateEngine templateEngine = new TemplateEngine();
        ServletContextHandler handler = new  ServletContextHandler();
        //List<Profile> profileList = List.of(new Profile("1","Tim","Cook","https://scontent.fiev9-1.fna.fbcdn.net/v/t1.6435-9/100063325_3065270396867687_1369217968893853696_n.jpg?_nc_cat=109&ccb=1-7&_nc_sid=8bfeb9&_nc_ohc=HtIe2ueOHTgAX9WBur-&_nc_ht=scontent.fiev9-1.fna&oh=00_AfDSqfrydSyDOpcbnMqwwnoc2YZ8vbZcn43BMqU2OxcFJg&oe=64096BD8"),new Profile("2","Gordon","Freeman","https://i.pinimg.com/736x/6a/97/50/6a975048e4b0e5a5f9c497af10551c48.jpg"),new Profile("3","Paul","Newman","https://scontent.fiev9-1.fna.fbcdn.net/v/t1.6435-9/100063325_3065270396867687_1369217968893853696_n.jpg?_nc_cat=109&ccb=1-7&_nc_sid=8bfeb9&_nc_ohc=HtIe2ueOHTgAX9WBur-&_nc_ht=scontent.fiev9-1.fna&oh=00_AfDSqfrydSyDOpcbnMqwwnoc2YZ8vbZcn43BMqU2OxcFJg&oe=64096BD8"));
        // List<Message> messageList = List.of(new Message(1L,profileList.get(0),"hello gfg"),new Message(2L,profileList.get(1),"hello 2"),new Message(3L,profileList.get(2),"hello 3"));

        JdbcMessageDao jdbcMessageDao = new JdbcMessageDao();
        DefaultMessageService defaultMessageService = new DefaultMessageService(jdbcMessageDao);
        LikedDao likedDao = new JdbcLikedDao(new ArrayList<>());
        JdbcProfileDao jdbcProfileDao = new JdbcProfileDao();
        DefaultProfileService defaultProfileService = new DefaultProfileService(jdbcProfileDao);
        DefaultLikedService defaultLikedService = new DefaultLikedService(likedDao);
        ProfileServlet helloServlet = new ProfileServlet(templateEngine,defaultLikedService,defaultProfileService);
        LikedServlet likedServlet = new LikedServlet(templateEngine,defaultLikedService);
        MessagesServlet messagesServlet = new MessagesServlet(templateEngine,defaultMessageService,defaultProfileService);
        LoginServlet loginServlet = new LoginServlet(templateEngine);
        handler.addServlet( new ServletHolder(helloServlet),"/users");
        handler.addServlet( new ServletHolder(likedServlet),"/liked");
        handler.addServlet(new ServletHolder(messagesServlet),"/messages" );
        handler.addServlet( new ServletHolder(loginServlet),"/");
        handler.addServlet(CssBootstrapServlet.class, "/assets/css/bootstrap.min.css");

       server.setHandler(handler);
       server.start();
       server.join();
    }
}
