package com.example.step6.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import com.example.step6.domain.Profile;
import org.postgresql.ds.PGPoolingDataSource;
interface ProfileDao {

     List<Profile> findAll();
     List <Profile>  findByIndex(Long index);


    default Connection  getConnection()throws SQLException {
        PGPoolingDataSource source = new PGPoolingDataSource();

        source.setServerName("localhost:5432");
        source.setDatabaseName("tinderapp");
        source.setUser("postgres");
        source.setPassword("root");
        source.setMaxConnections(10);
         source.getConnection();
        return source.getConnection();


    }
}
