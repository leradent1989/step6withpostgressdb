package com.example.step6.domain;

import java.util.List;

public class Profile {
    private Long id;
    private String login;

    private String password;
    private String name;
    private String surname;
    private String imgUrl;
    private List <Message> chat;


    public Profile(Long id,String name,String surname,String imgUrl) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.imgUrl = imgUrl;

    }
    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }
    public List <Message> getChat() {
        return chat;
    }

    public void setChat(List<Message> chat) {
        this.chat = chat;
    }
    @Override
    public String toString() {
        return "Profile{" +
                "id='" + id + '\'' +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", imgUrl='" + imgUrl + '\'' +
                '}';
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
