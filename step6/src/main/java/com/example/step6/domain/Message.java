package com.example.step6.domain;

public class Message {

    Profile authorProfile;
    Long userId;
    Long authorId;
    String text;

    public Profile getAuthorProfile() {
        return authorProfile;
    }

    public void setAuthorProfile(Profile authorProfile) {
        this.authorProfile = authorProfile;
    }

    public Message(Long userId, Long authorId, String text ) {
        this.userId = userId;
        this.authorId = authorId;
        this.text = text;

    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
    public Long getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Long authorId) {
        this.authorId = authorId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return "Message{" +
                "userId=" + userId +
                ", authorId=" + authorId +
                ", text='" + text + '\'' +
                '}';
    }
}
