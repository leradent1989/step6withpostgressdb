package com.example.step6.controllers;

import com.example.step6.domain.Message;
import com.example.step6.domain.TemplateEngine;
import com.example.step6.service.DefaultMessageService;
import com.example.step6.service.DefaultProfileService;
import com.example.step6.service.MessageService;
import com.example.step6.service.ProfileService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;

public class MessagesServlet extends HttpServlet {
    TemplateEngine templateEngine;
    MessageService defaultMessageService;
    ProfileService  defaultProfileService;

    public MessagesServlet(TemplateEngine templateEngine,DefaultMessageService defaultMessageService,DefaultProfileService defaultProfileService) {
        this.templateEngine = templateEngine;
        this.defaultMessageService = defaultMessageService;
        this.defaultProfileService = defaultProfileService;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

      //  templateEngine.render("chat.html",resp);
      String id =   req.getParameter("id");
       Long idLong = (long) Integer.parseInt(id);
        List < Message > messages = defaultMessageService.findByProfile(idLong);

        messages.forEach(el -> el.setAuthorProfile(defaultProfileService.findByIndex(el.getAuthorId()).get(0)));
   System.out.println("messages!" + messages );
        Map<String, Object> params = Map.of(
                //"profiles", List.of(new Profile("Tim","Cook","https://scontent.fiev9-1.fna.fbcdn.net/v/t1.6435-9/100063325_3065270396867687_1369217968893853696_n.jpg?_nc_cat=109&ccb=1-7&_nc_sid=8bfeb9&_nc_ohc=HtIe2ueOHTgAX9WBur-&_nc_ht=scontent.fiev9-1.fna&oh=00_AfDSqfrydSyDOpcbnMqwwnoc2YZ8vbZcn43BMqU2OxcFJg&oe=64096BD8"))
              //  "messages", defaultMessageService.findByProfile(Long.getLong(req.getParameter("id")))
                  "messages", messages,
                "messageForm",List.of(messages.get(0))
        );

        templateEngine.render("chat.html",params,resp);
    }
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String text = req.getParameter("new_message");

        String id =   req.getParameter("to");
        Long userId = (long) Integer.parseInt(id);
        List < Message > messages = defaultMessageService.findByProfile(userId);

        System.out.println(text);
        Random random = new Random();
        Long authorId = random.nextLong(0,3)+ 1L;

        Long idMessage = (long) (defaultMessageService.findAll().size() + 1);

        System.out.println(idMessage);

        Message message = new Message(userId,authorId,text);
        System.out.println(message);


        defaultMessageService.addNewMessage(idMessage,new Message(userId,authorId,text));

       resp.sendRedirect("/messages?id=" + userId);



    }
}
