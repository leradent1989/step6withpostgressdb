package com.example.step6.controllers;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import com.example.step6.domain.TemplateEngine;
import com.example.step6.service.DefaultLikedService;
import com.example.step6.service.DefaultProfileService;
import com.example.step6.service.LikedService;
import com.example.step6.service.ProfileService;

import java.util.Map;


public class ProfileServlet extends HttpServlet {
    public TemplateEngine templateEngine;
    //LikedDao likedDao ;

    public ProfileService defaultProfileService;

    LikedService defaultLikedService;

    public ProfileServlet(TemplateEngine templateEngine, DefaultLikedService defaultLikedService, DefaultProfileService defaultProfileService) {
        this.templateEngine = templateEngine;
        this.defaultLikedService =defaultLikedService;
        this.defaultProfileService = defaultProfileService;
    }
    //public List  <Profile> profileList = List.of(new Profile("1","Tim","Cook","https://scontent.fiev9-1.fna.fbcdn.net/v/t1.6435-9/100063325_3065270396867687_1369217968893853696_n.jpg?_nc_cat=109&ccb=1-7&_nc_sid=8bfeb9&_nc_ohc=HtIe2ueOHTgAX9WBur-&_nc_ht=scontent.fiev9-1.fna&oh=00_AfDSqfrydSyDOpcbnMqwwnoc2YZ8vbZcn43BMqU2OxcFJg&oe=64096BD8"),new Profile("2","Gordon","Freeman","https://i.pinimg.com/736x/6a/97/50/6a975048e4b0e5a5f9c497af10551c48.jpg"),new Profile("3","Paul","Newman","https://scontent.fiev9-1.fna.fbcdn.net/v/t1.6435-9/100063325_3065270396867687_1369217968893853696_n.jpg?_nc_cat=109&ccb=1-7&_nc_sid=8bfeb9&_nc_ohc=HtIe2ueOHTgAX9WBur-&_nc_ht=scontent.fiev9-1.fna&oh=00_AfDSqfrydSyDOpcbnMqwwnoc2YZ8vbZcn43BMqU2OxcFJg&oe=64096BD8"));
    Long count ;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");

        resp.getWriter().println("""
           
            <h2>Hello from servlet</h2>
            
        """);
        count=1L;




            Map<String, Object> params = Map.of(
                    //"profiles", List.of(new Profile("Tim","Cook","https://scontent.fiev9-1.fna.fbcdn.net/v/t1.6435-9/100063325_3065270396867687_1369217968893853696_n.jpg?_nc_cat=109&ccb=1-7&_nc_sid=8bfeb9&_nc_ohc=HtIe2ueOHTgAX9WBur-&_nc_ht=scontent.fiev9-1.fna&oh=00_AfDSqfrydSyDOpcbnMqwwnoc2YZ8vbZcn43BMqU2OxcFJg&oe=64096BD8"))
                    "profiles",   defaultProfileService.findByIndex(count)
                    );

            templateEngine.render("profile2.html", params, resp);
        }


    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
       String param1 = req.getParameter("param1");
        System.out.println(param1);
        String param2 = req.getParameter("param2");
        System.out.println(param2);
        if(param1 == null){
            param1 ="No";
        }
        if(param1.equals("YES")){
         defaultLikedService.addToList(defaultProfileService.findByIndex(count).get(0));
        }
     if(count > defaultProfileService.findAll().size() -1){
         count=1L;
         String path = req.getContextPath() + "/liked";
         resp.sendRedirect(path);


     }
         Map<String, Object> params = Map.of(
                 "profiles", defaultProfileService.findByIndex(++count)
         );
         templateEngine.render("profile2.html", params, resp);

    }
}
