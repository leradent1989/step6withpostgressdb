package com.example.step6.controllers;

import com.example.step6.domain.TemplateEngine;
import com.example.step6.service.DefaultLikedService;
import com.example.step6.service.LikedService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

public class LikedServlet extends HttpServlet {
    public TemplateEngine templateEngine;

//LikedDao likedDao = new LikedDao(new ArrayList<>());
   LikedService defaultLikedService;
    public LikedServlet(TemplateEngine templateEngine,DefaultLikedService defaultLikedService) {

        this.templateEngine = templateEngine;
        this.defaultLikedService =defaultLikedService;
    }



    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        resp.getWriter().println("""
           
            <h2>Liked profiles</h2>
            
        """);


        Map<String, Object> params = Map.of(
                //"profiles", List.of(new Profile("Tim","Cook","https://scontent.fiev9-1.fna.fbcdn.net/v/t1.6435-9/100063325_3065270396867687_1369217968893853696_n.jpg?_nc_cat=109&ccb=1-7&_nc_sid=8bfeb9&_nc_ohc=HtIe2ueOHTgAX9WBur-&_nc_ht=scontent.fiev9-1.fna&oh=00_AfDSqfrydSyDOpcbnMqwwnoc2YZ8vbZcn43BMqU2OxcFJg&oe=64096BD8"))
                "profiles", defaultLikedService.findAll()
        );

        templateEngine.render("liked.html",params,resp);
    }



}
