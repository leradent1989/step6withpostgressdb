package com.example.step6.service;

import com.example.step6.domain.Profile;

import java.util.List;

public interface ProfileService {
    List<Profile> findAll();
    List <Profile> findByIndex(Long index);

}
